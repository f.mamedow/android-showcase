package fm.android.showcase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.TextView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputLayout
import me.toptas.fancyshowcase.FancyShowCaseView
import me.toptas.fancyshowcase.FocusShape
import me.toptas.fancyshowcase.listener.DismissListener

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        showCaseSearchLayout()
    }

    private fun showCaseSearchLayout() {
        val searchLayout: TextInputLayout = findViewById(R.id.layout_filter)
        showCase(
            title = "Search Input",
            focusView = searchLayout,
            shape = FocusShape.ROUNDED_RECTANGLE,
            onDismissListener = ::showCaseDescription
        )
    }

    private fun showCaseFab() {
        val fab: FloatingActionButton = findViewById(R.id.fab_search)
        showCase(
            title = "Search Button",
            shape = FocusShape.CIRCLE,
            focusView = fab,
            onDismissListener = ::showCaseFinal
        )
    }

    private fun showCaseFinal() {
        showCase(
            title = "No Focus",
            focusView = null,
            shape = FocusShape.CIRCLE,
        )
    }

    private fun showCaseDescription() {
        val description: TextView = findViewById(R.id.tv_description)
        showCase(
            title = "Description Text",
            focusView = description,
            shape = FocusShape.CIRCLE,
            onDismissListener = ::showCaseFab,
        )
    }


    private fun showCase(
        title: String,
        focusView: View?,
        shape: FocusShape,
        onDismissListener: () -> Unit = {},
    ) {
        val builder = FancyShowCaseView.Builder(this)
        focusView?.let { builder.focusOn(it) }
        builder
            .title(title)
            .enableAutoTextPosition()
            .focusShape(shape)
            .dismissListener(object : DismissListener {
                override fun onDismiss(id: String?) {
                    onDismissListener()
                }

                override fun onSkipped(id: String?) {
                    onDismissListener()
                }

            })
            .build()
            .show()
    }
}